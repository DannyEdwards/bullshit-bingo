import React from "react";
import ReactDOM from "react-dom/client";
import Game from "./components/Game";
import "./index.css";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <Game rows={3} columns={3} />
  </React.StrictMode>
);
