import { useMemo, useState } from "react";
import BingoScreen from "./BingoScreen";
import { calculateBingo } from "./helpers";
import Square, { State as SquareState } from "./Square";

type Props = {
  initialState: SquareState[][];
};

export default function Board({ initialState }: Props) {
  const [board, setBoard] = useState(initialState);
  const bingo = useMemo(() => calculateBingo(board), [board]);

  function handleClick(row: number, column: number) {
    const newBoard = board.map((arr) => arr.slice());
    if (newBoard[row][column].selected || bingo.length) return;
    newBoard[row][column].selected = true;
    setBoard(newBoard);
  }

  function renderSquare(row: number, column: number, bingo: number[][]) {
    let bingoed = false;
    if (bingo.length) {
      for (let i = 0; i < bingo.length; i++) {
        bingoed =
          bingo[i][0] === row && bingo[i][1] === column ? true : bingoed;
      }
    }

    return (
      <Square
        key={`${row}-${column}`}
        selected={board[row][column].selected}
        bingoed={bingoed}
        onClick={() => handleClick(row, column)}
      >
        {board[row][column].phrase}
      </Square>
    );
  }

  return (
    <>
      <h1 className="title">Bullshit Bingo</h1>
      {bingo.length ? <BingoScreen /> : null}
      <div
        className="board"
        style={{
          gridTemplateColumns: "repeat(" + board.length + ", 1fr)",
          gridTemplateRows: "repeat(" + board.length + ", 1fr)",
        }}
      >
        {board.map((row, rowIndex) =>
          row.map((_, colIndex) => renderSquare(rowIndex, colIndex, bingo))
        )}
      </div>
    </>
  );
}
