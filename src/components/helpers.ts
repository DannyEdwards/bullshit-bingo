import type { State as SquareState } from "./Square";

export function calculateBingo(board: SquareState[][]) {
  let selected = board.map((row) => row.map((col) => col.selected));
  const columnLines = checkLines(selected);
  return columnLines.length ? columnLines : checkLines(selected, true);
}

export function checkLines(array: any[][], checkRows?: boolean) {
  let bingoLine = [];
  for (let i = 0; i < array.length; i++) {
    let test;
    for (let j = 0; j < array[i].length; j++) {
      test = checkRows ? array[i][j] : array[j][i];
      if (!test) {
        bingoLine = [];
        break;
      }
      checkRows ? bingoLine.push([i, j]) : bingoLine.push([j, i]);
    }
    if (bingoLine.length) return bingoLine;
  }
  return bingoLine;
}

export function shuffle(array: any[]) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}
