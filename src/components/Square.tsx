import { MouseEventHandler } from "react";

export type State = {
  index: number;
  phrase: string;
  selected: boolean;
};

type Props = Pick<State, "selected"> & {
  bingoed: boolean;
  children: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
};

export default function Square({
  bingoed,
  children,
  onClick,
  selected,
}: Props) {
  return (
    <button
      className={`square ${selected && "selected"} ${bingoed && "bingoed"}`}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
