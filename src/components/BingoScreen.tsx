export default function BingoScreen() {
  return (
    <div className="bingo-screen" onClick={() => window.location.reload()}>
      <h2>Zat's a bingo!</h2>
      <p>Click to reload</p>
    </div>
  );
}
