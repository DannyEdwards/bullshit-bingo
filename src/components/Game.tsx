import phrases from "../phrases.json";
import Board from "./Board";
import { shuffle } from "./helpers";

type Props = {
  rows: number;
  columns: number;
};

export default function Game({ rows, columns }: Props) {
  const shuffledPhrases = shuffle(phrases);
  const board = Array(rows)
    .fill(null)
    .map((_, i) =>
      Array(columns)
        .fill(null)
        .map((_, j) => ({
          index: i * columns + j,
          selected: false,
          phrase: shuffledPhrases[i * columns + j],
        }))
    );

  return (
    <div className="game">
      <Board initialState={board} />
    </div>
  );
}
